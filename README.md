Vous pouvez aller sur la branche boilerplate pour avoir un début de projet vierge

## How To Use
1. `git clone https://gitlab.com/simplonlyon/promo12/js-exercise-webpack.git`
2. `cd js-exercise-webpack`
3. `npm install`
4. `npm run dev`

## Exercice POO / Webpack
### I. Mise en place
1. Créer un nouveau projet npm js-exercice-webpack
2. Dans ce projet, créer le structure de dossier standard, installer webpack et créer un fichier de configuration avec juste ce qu'il faut pour qu'il soit en mode development avec les source-maps qui vont bien
3. Créer un script dev dans le package.json qui lancera la compilation webpack en mode watch
4. Créer votre point d'entrée js, votre fichier HTML et charger le bundle dedans
### II. La classe Character
1. Créer un fichier contenant une classe Character ayant comme propriétés : health (number), hunger (number), name (string) et une propriété position qui sera un objet avec x (number) et y (number) initialisés à zéro
2. Créer une méthode move(direction) qui selon si elle reçoit 'up', 'down', 'left' ou 'right' modifiera la position x/y de l'instance
3. Créer une méthode draw qui générera le HTML du personnage, comme on l'a déjà fait, à vous de voir à quoi vous voulez qu'il ressemble (pourquoi pas un carré rouge ?), dans tous les cas il devra afficher ses stat en dessous de lui, sera en position absolute et aura sa position y en top et sa position x en left
4. Faire une instance de Character dans le index.js, déclencher sa méthode draw et sa méthode move
### III. Automatiser le draw
1. Dans la classe Character, rajouter une propriété element avec null en valeur par défaut
2. Modifier la méthode draw pour faire que si la propriété element est null, il fait le createElement et le met dans la propriété, sinon faire qu'il remette à zéro le innerHTML de la propriété element
3. Faire un appel de la méthode draw() à la fin de la méthode move()
### IV. Contrôler le personnage
1. Créer un dossier src/controls et dedans créer un fichier ArrowKey.js
2. Dans ce fichier, créer (et exporter) une classe ArrowKey qui aura un Character en propriété
3. Créer une méthode init dans cette classe qui ajoutera un event listener au keydown sur l'élément window
(attention, on devra utiliser this dans cet event listener, donc bien faire une fat arrow function)
4. Dans l'event listener, utiliser l'argument event de la fonction pour faire en sorte de déclencher la méthode move de la propriété character avec l'argument qui va bien selon la touche qui a été pressée
en gros il va falloir récupérer la valeur de event.key ou event.code et via des if, déclencher la méthode this.character.move() en lui donnant 'up' si jamais le event.key est ArrowUp, ou down si ArrowDown etc.
5. Dans le index.js importer la classe ArrowKey et en faire une instance en lui donnant notre instance en argument, puis lancer sa méthode init()
### V. Les items et les ramasser
1. Créer une nouvelle classe Item qui aura comme propriété un name, un x et un y, et une cssClass (qui sera "default-item" par défaut)
2. Cette classe aura une méthode use(character) qui acceptera un personnage en argument et qui fera un return d'une chaîne de character en mode "le personnage .... utilise l'item ..."
3. Faire la méthode draw() de l'item qui fera peu ou prou comme celle du personnage, à la différence qu'on mettra juste "?" en textContent car on va dire qu'on ne sait pas quels sont les items au sol. Faire en sorte d'assigner la classe "item" et la classe contenue dans la propriété cssClass à l'élément représentant l'item
4. Dans la classe Character, rajouter une propriété backpack qui sera un tableau vide
5. Rajoute dans Character une méthode takeItem(item) qui attendra un item en argument et qui l'ajoutera dans le tableau backpack
### VI. Prendre le machin
1. Dans la classe Character, on va modifier la méthode takeItem, en rajoutant une boucle sur gameObjects à l'intérieur (qui sera importé de index.js)
2. Dans cette boucle, on va vérifier pour chaque élément du tableau si :
* L'élément en question est une instance de Item (utiliser instanceof)
* La position de l'élément en question correspond à la position de l'instance actuelle
3. Si la condition est vérifiée, on assigne l'élément actuel de la boucle à la variable item et on casse la boucle
4. Rajouter un if autour de la boucle pour faire en sorte de la faire seulement si le paramètre item est vide/falsy
5. Dans la classe ArrowKey, dans la méthode init, rajouter un nouveau if qui vérifie si la touche espace est pressée, si oui, on déclenche la méthode takeItem du personnage, sans argument
6. Dans takeItem, rajouter en dessous du push une ligne de code qui va faire un .remove() sur la propriété element de l'item qu'on push
### VII. Afficher l'inventaire
1. Dans la class Character, rajouter une propriété showBackpack à false par défaut
2. Créer une méthode toggleBackpack() qui va passer la propriété showBackpack de true à false ou false à true puis relancer la méthode draw
3. Modifier la méthode draw pour y rajouter une condition qui fera un truc si la propriété showBackpack est true
4. Dans cette condition : 
* Créer un nouvel élément div avec une classe css backpack
* Faire une boucle for...of sur la propriété backpack du personnage
* Pour chaque tour de boucle créer un paragraphe auquel vous mettrez comme textContent la propriété name de l'itérateur
* Rajouter un event au click sur le paragraphe (avec une fat arrow) qui déclenchera la méthode useItem avec comme argument l'itérateur
* Faire un append du paragraphe sur la div backpack
* Faire un append de la div backpack sur la div représentant le personnage
5. Faire une ptite class css .backpack pour faire l'affichage du sac à dos
6. Dans la classe ArrowKey, rajouter une nouvelle condition dans le init qui déclenchera la méthode openBackpack() du personnage quand on appuie la touche i