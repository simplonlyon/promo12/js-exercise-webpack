/**
 * En JS modulaire, pour pouvoir utiliser la classe Character
 * dans notre fichier index, on doit d'abord importer la
 * classe en question de son fichier
 */
import { Character } from "./Character";
import { ArrowKey } from "./controls/ArrowKey";
import { Item } from "./Item";
import { Food } from "./Food";
import { Pharma } from "./Pharma";

export const tileSize = 100;
//On fait une instance de personnage
let perso1 = new Character('Perso 1');
//Tableau contenant tous les objets de notre jeu actuel
export const gameObjects = [
    perso1,
    new Pharma('Doliprome', 4, 2, 30),
    new Food('bread', 5, 5, 100)
];
//On fait une boucle pour initialiser le draw de tous les objets en question
for (const iterator of gameObjects) {
    document.body.appendChild(iterator.draw())
}


//on fait une instance des contrôles au clavier en lui
//donnant l'instance de personnage à contrôler
let arrowKey = new ArrowKey(perso1);
//On initialise les contrôles au clavier
arrowKey.init();
//On fait un draw initial pour append le html du personnage où on veut

