import { tileSize, gameObjects } from ".";
import { Item } from "./Item";

/**
 * Pour que cette classe soit importable dans un autre
 * fichier, on doit l'indiquer avec le mot clef export
 */
export class Character {

    /**
     * @param {string} name 
     */
    constructor(name) {
        this.name = name;
        this.health = 100;
        this.hunger = 0;
        this.position = {
            x: 0,
            y: 0
        };
        /**
         * @type Element
         */
        this.element = null;
        /**
         * @type Item[]
         */
        this.backpack = [];
        this.showBackpack = false;
    }
    /**
     * Méthode qui déplace le personnage dans une direction donnée
     * @param {string} direction la direction dans laquelle va le personnage (doit être up, down, left ou right)
     */
    move(direction) {
        // if (direction === 'up') {
        //     this.position.y--;
        // }
        // if (direction === 'down') {
        //     this.position.y++;
        // }
        // if (direction === 'left') {
        //     this.position.x--;
        // }
        // if (direction === 'right') {
        //     this.position.x++;
        // }
        switch (direction) {
            case 'up':
                this.position.y--;
                break;
            case 'down':
                this.position.y++;
                break;
            case 'left':
                this.position.x--;
                break;
            case 'right':
                this.position.x++;
                break;

        }
        this.metabolism();
        this.draw();
    }
    /**
     * Méthode qui fait augmenter la faim du personnage et qui lui
     * fait baisser sa vie si jamais il a trop faim
     */
    metabolism() {
        if (this.hunger < 100) {
            this.hunger += 2;
        } else {
            this.health--;
        }
    }
    /**
     * Méthode qui "ouvre" ou "ferme" le sac du personnage
     */
    toggleBackpack(){
        this.showBackpack = !this.showBackpack;
        this.draw();
    }

    /**
     * Méthode qui met un item dans l'sac ou qui ramasse l'item à la
     * même position que le personnage si jamais on lui fournit 
     * pas d'item en paramètre
     * @param {Item} item l'item à mettre dans le sac
     */
    takeItem(item) {
        //Si l'argument item est vide
        if (!item) {
            //on fait une boucle sur tous les élément du jeu
            for (const iterator of gameObjects) {
                //On vérifie si la valeur actuelle de l'itérateur est une
                //instance de la classe Item
                if (iterator instanceof Item) {
                    //Si oui, on vérifie si sa position correspond à celle du personnage
                    if (this.position.x === iterator.position.x &&
                        this.position.y === iterator.position.y) {
                        //Si oui, on assigne cette valeur au paramètre item et on casse la boucle
                        item = iterator;
                        break;
                    }
                }
            }
        }
        //Si jamais un item a été trouvé
        if (item) {
            //On le met dans notre sac
            this.backpack.push(item);
            //On met sa position à une valeur null
            item.position = {}
            //on supprime l'élément html de l'item qu'on vient de ramasser
            item.element.remove();
        }        
    }

    /**
     * Méthode permettant au personnage d'utiliser un item
     * @param {Item} item L'item à utiliser
     */
    useItem(item) {
        alert(item.use(this));
    }

    /**
     * Méthode qui crée l'élément HTML du personnage
     * @returns {Element} l'élément HTML représentant le personnage
     */
    draw() {
        if (!this.element) {
            this.element = document.createElement('div');

        }
        let div = this.element;
        div.innerHTML = '';

        div.classList.add('character-style');

        this.drawStats(div);

        if(this.showBackpack) {
            this.drawBackpack(div);
        }

        div.style.top = this.position.y * tileSize + 'px';
        div.style.left = this.position.x * tileSize + 'px';

        return div;
    }
    /**
     * Méthode qui crée le HTML des statistiques du personnage
     * @param {Element} div L'élément où append les stats
     */
    drawStats(div) {
        let pName = document.createElement('p');
        pName.textContent = 'Name : ' + this.name;
        div.appendChild(pName);
        let pHealth = document.createElement('p');
        pHealth.textContent = 'Health : ' + this.health;
        div.appendChild(pHealth);
        let pHunger = document.createElement('p');
        pHunger.textContent = 'Hunger : ' + this.hunger;
        div.appendChild(pHunger);
    }
    /**
     * Méthode qui fait l'affichage de l'inventaire
     * @param {Element} div Element html sur lequel on append l'inventaire
     */
    drawBackpack(div) {
        let divBackpack = document.createElement('div');
        divBackpack.classList.add('backpack');
        for (const item of this.backpack) {
            let p = document.createElement('p');
            p.textContent = item.name;
            p.addEventListener('click', () => this.useItem(item));
            divBackpack.appendChild(p);
            //on peut alternativement utiliser le item.draw()
//            divBackpack.appendChild(item.draw());
        }

        div.appendChild(divBackpack);
    }
}