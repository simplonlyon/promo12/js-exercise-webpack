import { Item } from "./Item";
import { Character } from "./Character";


export class Pharma extends Item {
    /**
     * 
     * @param {string} name le nom du médoc
     * @param {number} x position du médoc x
     * @param {number} y position du médoc y
     * @param {number} effectivness efficacité du médoc
     */
    constructor(name, x, y, effectivness) {
        super(name, x, y, 'heal');
        this.effectivness = effectivness;
    }
    /**
     * @override
     * @param {Character} character le personnage qui utilise le médoc
     */
    use(character) {
        if(character.health === 100) {
            return character.name + ' already has full health';
        }
        character.health += this.effectivness;
        if(character.health > 100) {
            character.health = 100;
        }
        return super.use(character);
    }
}