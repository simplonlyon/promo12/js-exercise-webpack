import { Character } from "../Character";

export class ArrowKey {
    /**
     * 
     * @param {Character} character le personnage auquel on veut assigner des contrôles au clavier
     */
    constructor(character) {
        this.character = character;
    }

    init() {
        /**
         * On ajoute un event au keydown sur la window (sur le document ça
         * marche aussi). On fait attention à faire une fat arrow function
         * pour conserver la valeur du this à l'intérieur de l'event
         */
        window.addEventListener('keydown', event => {
            //Selon la touche qui est pressée, on déclenche la méthode
            //move avec une valeur ou une autre (techniquement on pourrait
            //refactoriser ça un peu mieux, mais ça le fera)
            if(event.key === 'ArrowUp') {
                this.character.move('up');
            }
            if(event.key === 'ArrowDown') {
                this.character.move('down');
            }
            if(event.key === 'ArrowLeft') {
                this.character.move('left');
            }
            if(event.key === 'ArrowRight') {
                this.character.move('right');
            }
            if(event.key === ' ') {
                this.character.takeItem();
            }
            if(event.key === 'i') {
                this.character.toggleBackpack();
                console.log(this.character);
            }
            
        });
    }
}