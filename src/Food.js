import { Item } from "./Item";
import { Character } from "./Character";
/**
 * L'héritage consiste à dire qu'une classe se base sur une autre
 * classe. Cela signifie que la classe enfant (celle qui hérite) possédera
 * toutes les méthodes et toutes les propriétés de son parent par défaut et
 * pourra définir de nouvelles méthodes ou propriétés qui lui sont propre.
 * Cela permet de réutiliser du code entre plusieurs classes. On utilise 
 * le mot clef extends suivi de la classe à hériter.
 * Ici notre classe Food possède toutes les propriétés de Item et toutes
 * ces méthodes : name, position, cssClass, element, draw et use
 * Mais également les propriétés et méthodes qui lui sont propres : 
 * calories et éventuellement cook()
 */
export class Food extends Item {
    /**
     * 
     * @param {string} name le nom de la nouriture
     * @param {number} x position x de la nourriture
     * @param {number} y position y de la nourriture
     * @param {number} calories les calories de la nourriture
     */
    constructor(name, x, y, calories) {
        //Dans le constructor d'un enfant, la première chose à faire est
        //d'appeler le constructor du parent avec le mot clef super
        super(name, x, y, 'food');
        //Une nouvelle propriété qui n'existera que dans les instances de food et pas celles de Item
        this.calories = calories;
    }
    /**
     * On redéfinit la méthode use du parent ce qui fait que lorsqu'on
     * appelera .use sur une instance de Food, c'est cette méthode là
     * qui sera appelée et pas celle de Item
     * @override
     * @param {Character} character 
     */
    use(character) {
        character.hunger -= this.calories;
        //On peut ceci dit faire appel à la méthode originale du
        //parent depuis une méthode de l'enfant
        return super.use(character);
    }
}
