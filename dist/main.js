/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Character.js":
/*!**************************!*\
  !*** ./src/Character.js ***!
  \**************************/
/*! exports provided: Character */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Character", function() { return Character; });
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! . */ "./src/index.js");
/* harmony import */ var _Item__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Item */ "./src/Item.js");



/**
 * Pour que cette classe soit importable dans un autre
 * fichier, on doit l'indiquer avec le mot clef export
 */
class Character {

    /**
     * @param {string} name 
     */
    constructor(name) {
        this.name = name;
        this.health = 100;
        this.hunger = 0;
        this.position = {
            x: 0,
            y: 0
        };
        /**
         * @type Element
         */
        this.element = null;
        /**
         * @type Item[]
         */
        this.backpack = [];
        this.showBackpack = false;
    }
    /**
     * Méthode qui déplace le personnage dans une direction donnée
     * @param {string} direction la direction dans laquelle va le personnage (doit être up, down, left ou right)
     */
    move(direction) {
        // if (direction === 'up') {
        //     this.position.y--;
        // }
        // if (direction === 'down') {
        //     this.position.y++;
        // }
        // if (direction === 'left') {
        //     this.position.x--;
        // }
        // if (direction === 'right') {
        //     this.position.x++;
        // }
        switch (direction) {
            case 'up':
                this.position.y--;
                break;
            case 'down':
                this.position.y++;
                break;
            case 'left':
                this.position.x--;
                break;
            case 'right':
                this.position.x++;
                break;

        }
        this.metabolism();
        this.draw();
    }
    /**
     * Méthode qui fait augmenter la faim du personnage et qui lui
     * fait baisser sa vie si jamais il a trop faim
     */
    metabolism() {
        if (this.hunger < 100) {
            this.hunger += 2;
        } else {
            this.health--;
        }
    }
    /**
     * Méthode qui "ouvre" ou "ferme" le sac du personnage
     */
    toggleBackpack(){
        this.showBackpack = !this.showBackpack;
        this.draw();
    }

    /**
     * Méthode qui met un item dans l'sac ou qui ramasse l'item à la
     * même position que le personnage si jamais on lui fournit 
     * pas d'item en paramètre
     * @param {Item} item l'item à mettre dans le sac
     */
    takeItem(item) {
        //Si l'argument item est vide
        if (!item) {
            //on fait une boucle sur tous les élément du jeu
            for (const iterator of ___WEBPACK_IMPORTED_MODULE_0__["gameObjects"]) {
                //On vérifie si la valeur actuelle de l'itérateur est une
                //instance de la classe Item
                if (iterator instanceof _Item__WEBPACK_IMPORTED_MODULE_1__["Item"]) {
                    //Si oui, on vérifie si sa position correspond à celle du personnage
                    if (this.position.x === iterator.position.x &&
                        this.position.y === iterator.position.y) {
                        //Si oui, on assigne cette valeur au paramètre item et on casse la boucle
                        item = iterator;
                        break;
                    }
                }
            }
        }
        //Si jamais un item a été trouvé
        if (item) {
            //On le met dans notre sac
            this.backpack.push(item);
            //On met sa position à une valeur null
            item.position = {}
            //on supprime l'élément html de l'item qu'on vient de ramasser
            item.element.remove();
        }        
    }

    /**
     * Méthode permettant au personnage d'utiliser un item
     * @param {Item} item L'item à utiliser
     */
    useItem(item) {
        alert(item.use(this));
    }

    /**
     * Méthode qui crée l'élément HTML du personnage
     * @returns {Element} l'élément HTML représentant le personnage
     */
    draw() {
        if (!this.element) {
            this.element = document.createElement('div');

        }
        let div = this.element;
        div.innerHTML = '';

        div.classList.add('character-style');

        this.drawStats(div);

        if(this.showBackpack) {
            this.drawBackpack(div);
        }

        div.style.top = this.position.y * ___WEBPACK_IMPORTED_MODULE_0__["tileSize"] + 'px';
        div.style.left = this.position.x * ___WEBPACK_IMPORTED_MODULE_0__["tileSize"] + 'px';

        return div;
    }
    /**
     * Méthode qui crée le HTML des statistiques du personnage
     * @param {Element} div L'élément où append les stats
     */
    drawStats(div) {
        let pName = document.createElement('p');
        pName.textContent = 'Name : ' + this.name;
        div.appendChild(pName);
        let pHealth = document.createElement('p');
        pHealth.textContent = 'Health : ' + this.health;
        div.appendChild(pHealth);
        let pHunger = document.createElement('p');
        pHunger.textContent = 'Hunger : ' + this.hunger;
        div.appendChild(pHunger);
    }
    /**
     * Méthode qui fait l'affichage de l'inventaire
     * @param {Element} div Element html sur lequel on append l'inventaire
     */
    drawBackpack(div) {
        let divBackpack = document.createElement('div');
        divBackpack.classList.add('backpack');
        for (const item of this.backpack) {
            let p = document.createElement('p');
            p.textContent = item.name;
            p.addEventListener('click', () => this.useItem(item));
            divBackpack.appendChild(p);
            //on peut alternativement utiliser le item.draw()
//            divBackpack.appendChild(item.draw());
        }

        div.appendChild(divBackpack);
    }
}

/***/ }),

/***/ "./src/Food.js":
/*!*********************!*\
  !*** ./src/Food.js ***!
  \*********************/
/*! exports provided: Food */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Food", function() { return Food; });
/* harmony import */ var _Item__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Item */ "./src/Item.js");
/* harmony import */ var _Character__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Character */ "./src/Character.js");


/**
 * L'héritage consiste à dire qu'une classe se base sur une autre
 * classe. Cela signifie que la classe enfant (celle qui hérite) possédera
 * toutes les méthodes et toutes les propriétés de son parent par défaut et
 * pourra définir de nouvelles méthodes ou propriétés qui lui sont propre.
 * Cela permet de réutiliser du code entre plusieurs classes. On utilise 
 * le mot clef extends suivi de la classe à hériter.
 * Ici notre classe Food possède toutes les propriétés de Item et toutes
 * ces méthodes : name, position, cssClass, element, draw et use
 * Mais également les propriétés et méthodes qui lui sont propres : 
 * calories et éventuellement cook()
 */
class Food extends _Item__WEBPACK_IMPORTED_MODULE_0__["Item"] {
    /**
     * 
     * @param {string} name le nom de la nouriture
     * @param {number} x position x de la nourriture
     * @param {number} y position y de la nourriture
     * @param {number} calories les calories de la nourriture
     */
    constructor(name, x, y, calories) {
        //Dans le constructor d'un enfant, la première chose à faire est
        //d'appeler le constructor du parent avec le mot clef super
        super(name, x, y, 'food');
        //Une nouvelle propriété qui n'existera que dans les instances de food et pas celles de Item
        this.calories = calories;
    }
    /**
     * On redéfinit la méthode use du parent ce qui fait que lorsqu'on
     * appelera .use sur une instance de Food, c'est cette méthode là
     * qui sera appelée et pas celle de Item
     * @override
     * @param {Character} character 
     */
    use(character) {
        character.hunger -= this.calories;
        //On peut ceci dit faire appel à la méthode originale du
        //parent depuis une méthode de l'enfant
        return super.use(character);
    }
}


/***/ }),

/***/ "./src/Item.js":
/*!*********************!*\
  !*** ./src/Item.js ***!
  \*********************/
/*! exports provided: Item */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Item", function() { return Item; });
/* harmony import */ var _Character__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Character */ "./src/Character.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! . */ "./src/index.js");



class Item {
    /**
     * 
     * @param {string} name le nom de l'item
     * @param {number} x position x de l'item
     * @param {number} y position y de l'item
     * @param {string} cssClass la classe css représentant cet objet
     */
    constructor(name, x, y, cssClass = 'default-class') {
        this.name = name;
        this.position = {
            x,
            y
        };
        this.cssClass = cssClass;
        /**
         * @type Element
         */
        this.element = null;
    }
    /**
     * Méthode qui sera appelée quand on utilise l'item
     * @param {Character} character Le personnage qui utilise l'item
     * @returns {string} La chaîne de caractère indiquant l'utilisation de l'item
     */
    use(character) {
        return `${character.name} used ${this.name} !`;
    }
    /**
     * 
     * @returns {Element} l'élément html représentant l'item
     */
    draw() {
        if(!this.element) {
            this.element = document.createElement('div');
            
        }
        let div = this.element;
        div.innerHTML = '';
        
        div.classList.add('item');
        div.classList.add(this.cssClass);
        div.textContent = '?';
        
        console.log(div);
        
        div.style.top = this.position.y * ___WEBPACK_IMPORTED_MODULE_1__["tileSize"] + 'px';
        div.style.left = this.position.x * ___WEBPACK_IMPORTED_MODULE_1__["tileSize"] + 'px';

        return div;
    }
}

/***/ }),

/***/ "./src/Pharma.js":
/*!***********************!*\
  !*** ./src/Pharma.js ***!
  \***********************/
/*! exports provided: Pharma */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Pharma", function() { return Pharma; });
/* harmony import */ var _Item__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Item */ "./src/Item.js");
/* harmony import */ var _Character__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Character */ "./src/Character.js");




class Pharma extends _Item__WEBPACK_IMPORTED_MODULE_0__["Item"] {
    /**
     * 
     * @param {string} name le nom du médoc
     * @param {number} x position du médoc x
     * @param {number} y position du médoc y
     * @param {number} effectivness efficacité du médoc
     */
    constructor(name, x, y, effectivness) {
        super(name, x, y, 'heal');
        this.effectivness = effectivness;
    }
    /**
     * @override
     * @param {Character} character le personnage qui utilise le médoc
     */
    use(character) {
        if(character.health === 100) {
            return character.name + ' already has full health';
        }
        character.health += this.effectivness;
        if(character.health > 100) {
            character.health = 100;
        }
        return super.use(character);
    }
}

/***/ }),

/***/ "./src/controls/ArrowKey.js":
/*!**********************************!*\
  !*** ./src/controls/ArrowKey.js ***!
  \**********************************/
/*! exports provided: ArrowKey */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ArrowKey", function() { return ArrowKey; });
/* harmony import */ var _Character__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Character */ "./src/Character.js");


class ArrowKey {
    /**
     * 
     * @param {Character} character le personnage auquel on veut assigner des contrôles au clavier
     */
    constructor(character) {
        this.character = character;
    }

    init() {
        /**
         * On ajoute un event au keydown sur la window (sur le document ça
         * marche aussi). On fait attention à faire une fat arrow function
         * pour conserver la valeur du this à l'intérieur de l'event
         */
        window.addEventListener('keydown', event => {
            //Selon la touche qui est pressée, on déclenche la méthode
            //move avec une valeur ou une autre (techniquement on pourrait
            //refactoriser ça un peu mieux, mais ça le fera)
            if(event.key === 'ArrowUp') {
                this.character.move('up');
            }
            if(event.key === 'ArrowDown') {
                this.character.move('down');
            }
            if(event.key === 'ArrowLeft') {
                this.character.move('left');
            }
            if(event.key === 'ArrowRight') {
                this.character.move('right');
            }
            if(event.key === ' ') {
                this.character.takeItem();
            }
            if(event.key === 'i') {
                this.character.toggleBackpack();
                console.log(this.character);
            }
            
        });
    }
}

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! exports provided: tileSize, gameObjects */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tileSize", function() { return tileSize; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "gameObjects", function() { return gameObjects; });
/* harmony import */ var _Character__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Character */ "./src/Character.js");
/* harmony import */ var _controls_ArrowKey__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./controls/ArrowKey */ "./src/controls/ArrowKey.js");
/* harmony import */ var _Item__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Item */ "./src/Item.js");
/* harmony import */ var _Food__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Food */ "./src/Food.js");
/* harmony import */ var _Pharma__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Pharma */ "./src/Pharma.js");
/**
 * En JS modulaire, pour pouvoir utiliser la classe Character
 * dans notre fichier index, on doit d'abord importer la
 * classe en question de son fichier
 */






const tileSize = 100;
//On fait une instance de personnage
let perso1 = new _Character__WEBPACK_IMPORTED_MODULE_0__["Character"]('Perso 1');
//Tableau contenant tous les objets de notre jeu actuel
const gameObjects = [
    perso1,
    new _Pharma__WEBPACK_IMPORTED_MODULE_4__["Pharma"]('Doliprome', 4, 2, 30),
    new _Food__WEBPACK_IMPORTED_MODULE_3__["Food"]('bread', 5, 5, 100)
];
//On fait une boucle pour initialiser le draw de tous les objets en question
for (const iterator of gameObjects) {
    document.body.appendChild(iterator.draw())
}


//on fait une instance des contrôles au clavier en lui
//donnant l'instance de personnage à contrôler
let arrowKey = new _controls_ArrowKey__WEBPACK_IMPORTED_MODULE_1__["ArrowKey"](perso1);
//On initialise les contrôles au clavier
arrowKey.init();
//On fait un draw initial pour append le html du personnage où on veut



/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjL0NoYXJhY3Rlci5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvRm9vZC5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvSXRlbS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvUGhhcm1hLmpzIiwid2VicGFjazovLy8uL3NyYy9jb250cm9scy9BcnJvd0tleS5qcyIsIndlYnBhY2s6Ly8vLi9zcmMvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBOzs7UUFHQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMENBQTBDLGdDQUFnQztRQUMxRTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLHdEQUF3RCxrQkFBa0I7UUFDMUU7UUFDQSxpREFBaUQsY0FBYztRQUMvRDs7UUFFQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EseUNBQXlDLGlDQUFpQztRQUMxRSxnSEFBZ0gsbUJBQW1CLEVBQUU7UUFDckk7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwyQkFBMkIsMEJBQTBCLEVBQUU7UUFDdkQsaUNBQWlDLGVBQWU7UUFDaEQ7UUFDQTtRQUNBOztRQUVBO1FBQ0Esc0RBQXNELCtEQUErRDs7UUFFckg7UUFDQTs7O1FBR0E7UUFDQTs7Ozs7Ozs7Ozs7OztBQ2xGQTtBQUFBO0FBQUE7QUFBQTtBQUEwQztBQUNaOztBQUU5QjtBQUNBO0FBQ0E7QUFDQTtBQUNPOztBQUVQO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsT0FBTztBQUN0QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsS0FBSztBQUNwQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsbUNBQW1DLDZDQUFXO0FBQzlDO0FBQ0E7QUFDQSx3Q0FBd0MsMENBQUk7QUFDNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsUztBQUNBOztBQUVBO0FBQ0E7QUFDQSxlQUFlLEtBQUs7QUFDcEI7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLGlCQUFpQixRQUFRO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsMENBQTBDLDBDQUFRO0FBQ2xELDJDQUEyQywwQ0FBUTs7QUFFbkQ7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLFFBQVE7QUFDdkI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsUUFBUTtBQUN2QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEM7Ozs7Ozs7Ozs7OztBQ3pMQTtBQUFBO0FBQUE7QUFBQTtBQUE4QjtBQUNVO0FBQ3hDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPLG1CQUFtQiwwQ0FBSTtBQUM5QjtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsT0FBTztBQUN0QixlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZUFBZSxVQUFVO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7QUMxQ0E7QUFBQTtBQUFBO0FBQUE7QUFBd0M7QUFDWDs7QUFFdEI7QUFDUDtBQUNBO0FBQ0EsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsT0FBTztBQUN0QixlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGVBQWUsVUFBVTtBQUN6QixpQkFBaUIsT0FBTztBQUN4QjtBQUNBO0FBQ0Esa0JBQWtCLGVBQWUsUUFBUSxVQUFVO0FBQ25EO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQixRQUFRO0FBQ3pCO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7O0FBRUEsMENBQTBDLDBDQUFRO0FBQ2xELDJDQUEyQywwQ0FBUTs7QUFFbkQ7QUFDQTtBQUNBLEM7Ozs7Ozs7Ozs7OztBQ3REQTtBQUFBO0FBQUE7QUFBQTtBQUE4QjtBQUNVOzs7QUFHakMscUJBQXFCLDBDQUFJO0FBQ2hDO0FBQ0E7QUFDQSxlQUFlLE9BQU87QUFDdEIsZUFBZSxPQUFPO0FBQ3RCLGVBQWUsT0FBTztBQUN0QixlQUFlLE9BQU87QUFDdEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxlQUFlLFVBQVU7QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEM7Ozs7Ozs7Ozs7OztBQzlCQTtBQUFBO0FBQUE7QUFBeUM7O0FBRWxDO0FBQ1A7QUFDQTtBQUNBLGVBQWUsVUFBVTtBQUN6QjtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLFNBQVM7QUFDVDtBQUNBLEM7Ozs7Ozs7Ozs7OztBQzNDQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUN3QztBQUNPO0FBQ2pCO0FBQ0E7QUFDSTs7QUFFM0I7QUFDUDtBQUNBLGlCQUFpQixvREFBUztBQUMxQjtBQUNPO0FBQ1A7QUFDQSxRQUFRLDhDQUFNO0FBQ2QsUUFBUSwwQ0FBSTtBQUNaO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQSxtQkFBbUIsMkRBQVE7QUFDM0I7QUFDQTtBQUNBIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyIgXHQvLyBUaGUgbW9kdWxlIGNhY2hlXG4gXHR2YXIgaW5zdGFsbGVkTW9kdWxlcyA9IHt9O1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIlwiO1xuXG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gXCIuL3NyYy9pbmRleC5qc1wiKTtcbiIsImltcG9ydCB7IHRpbGVTaXplLCBnYW1lT2JqZWN0cyB9IGZyb20gXCIuXCI7XG5pbXBvcnQgeyBJdGVtIH0gZnJvbSBcIi4vSXRlbVwiO1xuXG4vKipcbiAqIFBvdXIgcXVlIGNldHRlIGNsYXNzZSBzb2l0IGltcG9ydGFibGUgZGFucyB1biBhdXRyZVxuICogZmljaGllciwgb24gZG9pdCBsJ2luZGlxdWVyIGF2ZWMgbGUgbW90IGNsZWYgZXhwb3J0XG4gKi9cbmV4cG9ydCBjbGFzcyBDaGFyYWN0ZXIge1xuXG4gICAgLyoqXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IG5hbWUgXG4gICAgICovXG4gICAgY29uc3RydWN0b3IobmFtZSkge1xuICAgICAgICB0aGlzLm5hbWUgPSBuYW1lO1xuICAgICAgICB0aGlzLmhlYWx0aCA9IDEwMDtcbiAgICAgICAgdGhpcy5odW5nZXIgPSAwO1xuICAgICAgICB0aGlzLnBvc2l0aW9uID0ge1xuICAgICAgICAgICAgeDogMCxcbiAgICAgICAgICAgIHk6IDBcbiAgICAgICAgfTtcbiAgICAgICAgLyoqXG4gICAgICAgICAqIEB0eXBlIEVsZW1lbnRcbiAgICAgICAgICovXG4gICAgICAgIHRoaXMuZWxlbWVudCA9IG51bGw7XG4gICAgICAgIC8qKlxuICAgICAgICAgKiBAdHlwZSBJdGVtW11cbiAgICAgICAgICovXG4gICAgICAgIHRoaXMuYmFja3BhY2sgPSBbXTtcbiAgICAgICAgdGhpcy5zaG93QmFja3BhY2sgPSBmYWxzZTtcbiAgICB9XG4gICAgLyoqXG4gICAgICogTcOpdGhvZGUgcXVpIGTDqXBsYWNlIGxlIHBlcnNvbm5hZ2UgZGFucyB1bmUgZGlyZWN0aW9uIGRvbm7DqWVcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gZGlyZWN0aW9uIGxhIGRpcmVjdGlvbiBkYW5zIGxhcXVlbGxlIHZhIGxlIHBlcnNvbm5hZ2UgKGRvaXQgw6p0cmUgdXAsIGRvd24sIGxlZnQgb3UgcmlnaHQpXG4gICAgICovXG4gICAgbW92ZShkaXJlY3Rpb24pIHtcbiAgICAgICAgLy8gaWYgKGRpcmVjdGlvbiA9PT0gJ3VwJykge1xuICAgICAgICAvLyAgICAgdGhpcy5wb3NpdGlvbi55LS07XG4gICAgICAgIC8vIH1cbiAgICAgICAgLy8gaWYgKGRpcmVjdGlvbiA9PT0gJ2Rvd24nKSB7XG4gICAgICAgIC8vICAgICB0aGlzLnBvc2l0aW9uLnkrKztcbiAgICAgICAgLy8gfVxuICAgICAgICAvLyBpZiAoZGlyZWN0aW9uID09PSAnbGVmdCcpIHtcbiAgICAgICAgLy8gICAgIHRoaXMucG9zaXRpb24ueC0tO1xuICAgICAgICAvLyB9XG4gICAgICAgIC8vIGlmIChkaXJlY3Rpb24gPT09ICdyaWdodCcpIHtcbiAgICAgICAgLy8gICAgIHRoaXMucG9zaXRpb24ueCsrO1xuICAgICAgICAvLyB9XG4gICAgICAgIHN3aXRjaCAoZGlyZWN0aW9uKSB7XG4gICAgICAgICAgICBjYXNlICd1cCc6XG4gICAgICAgICAgICAgICAgdGhpcy5wb3NpdGlvbi55LS07XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlICdkb3duJzpcbiAgICAgICAgICAgICAgICB0aGlzLnBvc2l0aW9uLnkrKztcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgJ2xlZnQnOlxuICAgICAgICAgICAgICAgIHRoaXMucG9zaXRpb24ueC0tO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSAncmlnaHQnOlxuICAgICAgICAgICAgICAgIHRoaXMucG9zaXRpb24ueCsrO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5tZXRhYm9saXNtKCk7XG4gICAgICAgIHRoaXMuZHJhdygpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBNw6l0aG9kZSBxdWkgZmFpdCBhdWdtZW50ZXIgbGEgZmFpbSBkdSBwZXJzb25uYWdlIGV0IHF1aSBsdWlcbiAgICAgKiBmYWl0IGJhaXNzZXIgc2EgdmllIHNpIGphbWFpcyBpbCBhIHRyb3AgZmFpbVxuICAgICAqL1xuICAgIG1ldGFib2xpc20oKSB7XG4gICAgICAgIGlmICh0aGlzLmh1bmdlciA8IDEwMCkge1xuICAgICAgICAgICAgdGhpcy5odW5nZXIgKz0gMjtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuaGVhbHRoLS07XG4gICAgICAgIH1cbiAgICB9XG4gICAgLyoqXG4gICAgICogTcOpdGhvZGUgcXVpIFwib3V2cmVcIiBvdSBcImZlcm1lXCIgbGUgc2FjIGR1IHBlcnNvbm5hZ2VcbiAgICAgKi9cbiAgICB0b2dnbGVCYWNrcGFjaygpe1xuICAgICAgICB0aGlzLnNob3dCYWNrcGFjayA9ICF0aGlzLnNob3dCYWNrcGFjaztcbiAgICAgICAgdGhpcy5kcmF3KCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogTcOpdGhvZGUgcXVpIG1ldCB1biBpdGVtIGRhbnMgbCdzYWMgb3UgcXVpIHJhbWFzc2UgbCdpdGVtIMOgIGxhXG4gICAgICogbcOqbWUgcG9zaXRpb24gcXVlIGxlIHBlcnNvbm5hZ2Ugc2kgamFtYWlzIG9uIGx1aSBmb3Vybml0IFxuICAgICAqIHBhcyBkJ2l0ZW0gZW4gcGFyYW3DqHRyZVxuICAgICAqIEBwYXJhbSB7SXRlbX0gaXRlbSBsJ2l0ZW0gw6AgbWV0dHJlIGRhbnMgbGUgc2FjXG4gICAgICovXG4gICAgdGFrZUl0ZW0oaXRlbSkge1xuICAgICAgICAvL1NpIGwnYXJndW1lbnQgaXRlbSBlc3QgdmlkZVxuICAgICAgICBpZiAoIWl0ZW0pIHtcbiAgICAgICAgICAgIC8vb24gZmFpdCB1bmUgYm91Y2xlIHN1ciB0b3VzIGxlcyDDqWzDqW1lbnQgZHUgamV1XG4gICAgICAgICAgICBmb3IgKGNvbnN0IGl0ZXJhdG9yIG9mIGdhbWVPYmplY3RzKSB7XG4gICAgICAgICAgICAgICAgLy9PbiB2w6lyaWZpZSBzaSBsYSB2YWxldXIgYWN0dWVsbGUgZGUgbCdpdMOpcmF0ZXVyIGVzdCB1bmVcbiAgICAgICAgICAgICAgICAvL2luc3RhbmNlIGRlIGxhIGNsYXNzZSBJdGVtXG4gICAgICAgICAgICAgICAgaWYgKGl0ZXJhdG9yIGluc3RhbmNlb2YgSXRlbSkge1xuICAgICAgICAgICAgICAgICAgICAvL1NpIG91aSwgb24gdsOpcmlmaWUgc2kgc2EgcG9zaXRpb24gY29ycmVzcG9uZCDDoCBjZWxsZSBkdSBwZXJzb25uYWdlXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnBvc2l0aW9uLnggPT09IGl0ZXJhdG9yLnBvc2l0aW9uLnggJiZcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucG9zaXRpb24ueSA9PT0gaXRlcmF0b3IucG9zaXRpb24ueSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgLy9TaSBvdWksIG9uIGFzc2lnbmUgY2V0dGUgdmFsZXVyIGF1IHBhcmFtw6h0cmUgaXRlbSBldCBvbiBjYXNzZSBsYSBib3VjbGVcbiAgICAgICAgICAgICAgICAgICAgICAgIGl0ZW0gPSBpdGVyYXRvcjtcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIC8vU2kgamFtYWlzIHVuIGl0ZW0gYSDDqXTDqSB0cm91dsOpXG4gICAgICAgIGlmIChpdGVtKSB7XG4gICAgICAgICAgICAvL09uIGxlIG1ldCBkYW5zIG5vdHJlIHNhY1xuICAgICAgICAgICAgdGhpcy5iYWNrcGFjay5wdXNoKGl0ZW0pO1xuICAgICAgICAgICAgLy9PbiBtZXQgc2EgcG9zaXRpb24gw6AgdW5lIHZhbGV1ciBudWxsXG4gICAgICAgICAgICBpdGVtLnBvc2l0aW9uID0ge31cbiAgICAgICAgICAgIC8vb24gc3VwcHJpbWUgbCfDqWzDqW1lbnQgaHRtbCBkZSBsJ2l0ZW0gcXUnb24gdmllbnQgZGUgcmFtYXNzZXJcbiAgICAgICAgICAgIGl0ZW0uZWxlbWVudC5yZW1vdmUoKTtcbiAgICAgICAgfSAgICAgICAgXG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogTcOpdGhvZGUgcGVybWV0dGFudCBhdSBwZXJzb25uYWdlIGQndXRpbGlzZXIgdW4gaXRlbVxuICAgICAqIEBwYXJhbSB7SXRlbX0gaXRlbSBMJ2l0ZW0gw6AgdXRpbGlzZXJcbiAgICAgKi9cbiAgICB1c2VJdGVtKGl0ZW0pIHtcbiAgICAgICAgYWxlcnQoaXRlbS51c2UodGhpcykpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIE3DqXRob2RlIHF1aSBjcsOpZSBsJ8OpbMOpbWVudCBIVE1MIGR1IHBlcnNvbm5hZ2VcbiAgICAgKiBAcmV0dXJucyB7RWxlbWVudH0gbCfDqWzDqW1lbnQgSFRNTCByZXByw6lzZW50YW50IGxlIHBlcnNvbm5hZ2VcbiAgICAgKi9cbiAgICBkcmF3KCkge1xuICAgICAgICBpZiAoIXRoaXMuZWxlbWVudCkge1xuICAgICAgICAgICAgdGhpcy5lbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG5cbiAgICAgICAgfVxuICAgICAgICBsZXQgZGl2ID0gdGhpcy5lbGVtZW50O1xuICAgICAgICBkaXYuaW5uZXJIVE1MID0gJyc7XG5cbiAgICAgICAgZGl2LmNsYXNzTGlzdC5hZGQoJ2NoYXJhY3Rlci1zdHlsZScpO1xuXG4gICAgICAgIHRoaXMuZHJhd1N0YXRzKGRpdik7XG5cbiAgICAgICAgaWYodGhpcy5zaG93QmFja3BhY2spIHtcbiAgICAgICAgICAgIHRoaXMuZHJhd0JhY2twYWNrKGRpdik7XG4gICAgICAgIH1cblxuICAgICAgICBkaXYuc3R5bGUudG9wID0gdGhpcy5wb3NpdGlvbi55ICogdGlsZVNpemUgKyAncHgnO1xuICAgICAgICBkaXYuc3R5bGUubGVmdCA9IHRoaXMucG9zaXRpb24ueCAqIHRpbGVTaXplICsgJ3B4JztcblxuICAgICAgICByZXR1cm4gZGl2O1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBNw6l0aG9kZSBxdWkgY3LDqWUgbGUgSFRNTCBkZXMgc3RhdGlzdGlxdWVzIGR1IHBlcnNvbm5hZ2VcbiAgICAgKiBAcGFyYW0ge0VsZW1lbnR9IGRpdiBMJ8OpbMOpbWVudCBvw7kgYXBwZW5kIGxlcyBzdGF0c1xuICAgICAqL1xuICAgIGRyYXdTdGF0cyhkaXYpIHtcbiAgICAgICAgbGV0IHBOYW1lID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgncCcpO1xuICAgICAgICBwTmFtZS50ZXh0Q29udGVudCA9ICdOYW1lIDogJyArIHRoaXMubmFtZTtcbiAgICAgICAgZGl2LmFwcGVuZENoaWxkKHBOYW1lKTtcbiAgICAgICAgbGV0IHBIZWFsdGggPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdwJyk7XG4gICAgICAgIHBIZWFsdGgudGV4dENvbnRlbnQgPSAnSGVhbHRoIDogJyArIHRoaXMuaGVhbHRoO1xuICAgICAgICBkaXYuYXBwZW5kQ2hpbGQocEhlYWx0aCk7XG4gICAgICAgIGxldCBwSHVuZ2VyID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgncCcpO1xuICAgICAgICBwSHVuZ2VyLnRleHRDb250ZW50ID0gJ0h1bmdlciA6ICcgKyB0aGlzLmh1bmdlcjtcbiAgICAgICAgZGl2LmFwcGVuZENoaWxkKHBIdW5nZXIpO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBNw6l0aG9kZSBxdWkgZmFpdCBsJ2FmZmljaGFnZSBkZSBsJ2ludmVudGFpcmVcbiAgICAgKiBAcGFyYW0ge0VsZW1lbnR9IGRpdiBFbGVtZW50IGh0bWwgc3VyIGxlcXVlbCBvbiBhcHBlbmQgbCdpbnZlbnRhaXJlXG4gICAgICovXG4gICAgZHJhd0JhY2twYWNrKGRpdikge1xuICAgICAgICBsZXQgZGl2QmFja3BhY2sgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdkaXYnKTtcbiAgICAgICAgZGl2QmFja3BhY2suY2xhc3NMaXN0LmFkZCgnYmFja3BhY2snKTtcbiAgICAgICAgZm9yIChjb25zdCBpdGVtIG9mIHRoaXMuYmFja3BhY2spIHtcbiAgICAgICAgICAgIGxldCBwID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgncCcpO1xuICAgICAgICAgICAgcC50ZXh0Q29udGVudCA9IGl0ZW0ubmFtZTtcbiAgICAgICAgICAgIHAuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB0aGlzLnVzZUl0ZW0oaXRlbSkpO1xuICAgICAgICAgICAgZGl2QmFja3BhY2suYXBwZW5kQ2hpbGQocCk7XG4gICAgICAgICAgICAvL29uIHBldXQgYWx0ZXJuYXRpdmVtZW50IHV0aWxpc2VyIGxlIGl0ZW0uZHJhdygpXG4vLyAgICAgICAgICAgIGRpdkJhY2twYWNrLmFwcGVuZENoaWxkKGl0ZW0uZHJhdygpKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGRpdi5hcHBlbmRDaGlsZChkaXZCYWNrcGFjayk7XG4gICAgfVxufSIsImltcG9ydCB7IEl0ZW0gfSBmcm9tIFwiLi9JdGVtXCI7XG5pbXBvcnQgeyBDaGFyYWN0ZXIgfSBmcm9tIFwiLi9DaGFyYWN0ZXJcIjtcbi8qKlxuICogTCdow6lyaXRhZ2UgY29uc2lzdGUgw6AgZGlyZSBxdSd1bmUgY2xhc3NlIHNlIGJhc2Ugc3VyIHVuZSBhdXRyZVxuICogY2xhc3NlLiBDZWxhIHNpZ25pZmllIHF1ZSBsYSBjbGFzc2UgZW5mYW50IChjZWxsZSBxdWkgaMOpcml0ZSkgcG9zc8OpZGVyYVxuICogdG91dGVzIGxlcyBtw6l0aG9kZXMgZXQgdG91dGVzIGxlcyBwcm9wcmnDqXTDqXMgZGUgc29uIHBhcmVudCBwYXIgZMOpZmF1dCBldFxuICogcG91cnJhIGTDqWZpbmlyIGRlIG5vdXZlbGxlcyBtw6l0aG9kZXMgb3UgcHJvcHJpw6l0w6lzIHF1aSBsdWkgc29udCBwcm9wcmUuXG4gKiBDZWxhIHBlcm1ldCBkZSByw6l1dGlsaXNlciBkdSBjb2RlIGVudHJlIHBsdXNpZXVycyBjbGFzc2VzLiBPbiB1dGlsaXNlIFxuICogbGUgbW90IGNsZWYgZXh0ZW5kcyBzdWl2aSBkZSBsYSBjbGFzc2Ugw6AgaMOpcml0ZXIuXG4gKiBJY2kgbm90cmUgY2xhc3NlIEZvb2QgcG9zc8OoZGUgdG91dGVzIGxlcyBwcm9wcmnDqXTDqXMgZGUgSXRlbSBldCB0b3V0ZXNcbiAqIGNlcyBtw6l0aG9kZXMgOiBuYW1lLCBwb3NpdGlvbiwgY3NzQ2xhc3MsIGVsZW1lbnQsIGRyYXcgZXQgdXNlXG4gKiBNYWlzIMOpZ2FsZW1lbnQgbGVzIHByb3ByacOpdMOpcyBldCBtw6l0aG9kZXMgcXVpIGx1aSBzb250IHByb3ByZXMgOiBcbiAqIGNhbG9yaWVzIGV0IMOpdmVudHVlbGxlbWVudCBjb29rKClcbiAqL1xuZXhwb3J0IGNsYXNzIEZvb2QgZXh0ZW5kcyBJdGVtIHtcbiAgICAvKipcbiAgICAgKiBcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gbmFtZSBsZSBub20gZGUgbGEgbm91cml0dXJlXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHggcG9zaXRpb24geCBkZSBsYSBub3Vycml0dXJlXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHkgcG9zaXRpb24geSBkZSBsYSBub3Vycml0dXJlXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IGNhbG9yaWVzIGxlcyBjYWxvcmllcyBkZSBsYSBub3Vycml0dXJlXG4gICAgICovXG4gICAgY29uc3RydWN0b3IobmFtZSwgeCwgeSwgY2Fsb3JpZXMpIHtcbiAgICAgICAgLy9EYW5zIGxlIGNvbnN0cnVjdG9yIGQndW4gZW5mYW50LCBsYSBwcmVtacOocmUgY2hvc2Ugw6AgZmFpcmUgZXN0XG4gICAgICAgIC8vZCdhcHBlbGVyIGxlIGNvbnN0cnVjdG9yIGR1IHBhcmVudCBhdmVjIGxlIG1vdCBjbGVmIHN1cGVyXG4gICAgICAgIHN1cGVyKG5hbWUsIHgsIHksICdmb29kJyk7XG4gICAgICAgIC8vVW5lIG5vdXZlbGxlIHByb3ByacOpdMOpIHF1aSBuJ2V4aXN0ZXJhIHF1ZSBkYW5zIGxlcyBpbnN0YW5jZXMgZGUgZm9vZCBldCBwYXMgY2VsbGVzIGRlIEl0ZW1cbiAgICAgICAgdGhpcy5jYWxvcmllcyA9IGNhbG9yaWVzO1xuICAgIH1cbiAgICAvKipcbiAgICAgKiBPbiByZWTDqWZpbml0IGxhIG3DqXRob2RlIHVzZSBkdSBwYXJlbnQgY2UgcXVpIGZhaXQgcXVlIGxvcnNxdSdvblxuICAgICAqIGFwcGVsZXJhIC51c2Ugc3VyIHVuZSBpbnN0YW5jZSBkZSBGb29kLCBjJ2VzdCBjZXR0ZSBtw6l0aG9kZSBsw6BcbiAgICAgKiBxdWkgc2VyYSBhcHBlbMOpZSBldCBwYXMgY2VsbGUgZGUgSXRlbVxuICAgICAqIEBvdmVycmlkZVxuICAgICAqIEBwYXJhbSB7Q2hhcmFjdGVyfSBjaGFyYWN0ZXIgXG4gICAgICovXG4gICAgdXNlKGNoYXJhY3Rlcikge1xuICAgICAgICBjaGFyYWN0ZXIuaHVuZ2VyIC09IHRoaXMuY2Fsb3JpZXM7XG4gICAgICAgIC8vT24gcGV1dCBjZWNpIGRpdCBmYWlyZSBhcHBlbCDDoCBsYSBtw6l0aG9kZSBvcmlnaW5hbGUgZHVcbiAgICAgICAgLy9wYXJlbnQgZGVwdWlzIHVuZSBtw6l0aG9kZSBkZSBsJ2VuZmFudFxuICAgICAgICByZXR1cm4gc3VwZXIudXNlKGNoYXJhY3Rlcik7XG4gICAgfVxufVxuIiwiaW1wb3J0IHsgQ2hhcmFjdGVyIH0gZnJvbSBcIi4vQ2hhcmFjdGVyXCI7XG5pbXBvcnQgeyB0aWxlU2l6ZSB9IGZyb20gXCIuXCI7XG5cbmV4cG9ydCBjbGFzcyBJdGVtIHtcbiAgICAvKipcbiAgICAgKiBcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gbmFtZSBsZSBub20gZGUgbCdpdGVtXG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHggcG9zaXRpb24geCBkZSBsJ2l0ZW1cbiAgICAgKiBAcGFyYW0ge251bWJlcn0geSBwb3NpdGlvbiB5IGRlIGwnaXRlbVxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSBjc3NDbGFzcyBsYSBjbGFzc2UgY3NzIHJlcHLDqXNlbnRhbnQgY2V0IG9iamV0XG4gICAgICovXG4gICAgY29uc3RydWN0b3IobmFtZSwgeCwgeSwgY3NzQ2xhc3MgPSAnZGVmYXVsdC1jbGFzcycpIHtcbiAgICAgICAgdGhpcy5uYW1lID0gbmFtZTtcbiAgICAgICAgdGhpcy5wb3NpdGlvbiA9IHtcbiAgICAgICAgICAgIHgsXG4gICAgICAgICAgICB5XG4gICAgICAgIH07XG4gICAgICAgIHRoaXMuY3NzQ2xhc3MgPSBjc3NDbGFzcztcbiAgICAgICAgLyoqXG4gICAgICAgICAqIEB0eXBlIEVsZW1lbnRcbiAgICAgICAgICovXG4gICAgICAgIHRoaXMuZWxlbWVudCA9IG51bGw7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIE3DqXRob2RlIHF1aSBzZXJhIGFwcGVsw6llIHF1YW5kIG9uIHV0aWxpc2UgbCdpdGVtXG4gICAgICogQHBhcmFtIHtDaGFyYWN0ZXJ9IGNoYXJhY3RlciBMZSBwZXJzb25uYWdlIHF1aSB1dGlsaXNlIGwnaXRlbVxuICAgICAqIEByZXR1cm5zIHtzdHJpbmd9IExhIGNoYcOubmUgZGUgY2FyYWN0w6hyZSBpbmRpcXVhbnQgbCd1dGlsaXNhdGlvbiBkZSBsJ2l0ZW1cbiAgICAgKi9cbiAgICB1c2UoY2hhcmFjdGVyKSB7XG4gICAgICAgIHJldHVybiBgJHtjaGFyYWN0ZXIubmFtZX0gdXNlZCAke3RoaXMubmFtZX0gIWA7XG4gICAgfVxuICAgIC8qKlxuICAgICAqIFxuICAgICAqIEByZXR1cm5zIHtFbGVtZW50fSBsJ8OpbMOpbWVudCBodG1sIHJlcHLDqXNlbnRhbnQgbCdpdGVtXG4gICAgICovXG4gICAgZHJhdygpIHtcbiAgICAgICAgaWYoIXRoaXMuZWxlbWVudCkge1xuICAgICAgICAgICAgdGhpcy5lbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnZGl2Jyk7XG4gICAgICAgICAgICBcbiAgICAgICAgfVxuICAgICAgICBsZXQgZGl2ID0gdGhpcy5lbGVtZW50O1xuICAgICAgICBkaXYuaW5uZXJIVE1MID0gJyc7XG4gICAgICAgIFxuICAgICAgICBkaXYuY2xhc3NMaXN0LmFkZCgnaXRlbScpO1xuICAgICAgICBkaXYuY2xhc3NMaXN0LmFkZCh0aGlzLmNzc0NsYXNzKTtcbiAgICAgICAgZGl2LnRleHRDb250ZW50ID0gJz8nO1xuICAgICAgICBcbiAgICAgICAgY29uc29sZS5sb2coZGl2KTtcbiAgICAgICAgXG4gICAgICAgIGRpdi5zdHlsZS50b3AgPSB0aGlzLnBvc2l0aW9uLnkgKiB0aWxlU2l6ZSArICdweCc7XG4gICAgICAgIGRpdi5zdHlsZS5sZWZ0ID0gdGhpcy5wb3NpdGlvbi54ICogdGlsZVNpemUgKyAncHgnO1xuXG4gICAgICAgIHJldHVybiBkaXY7XG4gICAgfVxufSIsImltcG9ydCB7IEl0ZW0gfSBmcm9tIFwiLi9JdGVtXCI7XG5pbXBvcnQgeyBDaGFyYWN0ZXIgfSBmcm9tIFwiLi9DaGFyYWN0ZXJcIjtcblxuXG5leHBvcnQgY2xhc3MgUGhhcm1hIGV4dGVuZHMgSXRlbSB7XG4gICAgLyoqXG4gICAgICogXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IG5hbWUgbGUgbm9tIGR1IG3DqWRvY1xuICAgICAqIEBwYXJhbSB7bnVtYmVyfSB4IHBvc2l0aW9uIGR1IG3DqWRvYyB4XG4gICAgICogQHBhcmFtIHtudW1iZXJ9IHkgcG9zaXRpb24gZHUgbcOpZG9jIHlcbiAgICAgKiBAcGFyYW0ge251bWJlcn0gZWZmZWN0aXZuZXNzIGVmZmljYWNpdMOpIGR1IG3DqWRvY1xuICAgICAqL1xuICAgIGNvbnN0cnVjdG9yKG5hbWUsIHgsIHksIGVmZmVjdGl2bmVzcykge1xuICAgICAgICBzdXBlcihuYW1lLCB4LCB5LCAnaGVhbCcpO1xuICAgICAgICB0aGlzLmVmZmVjdGl2bmVzcyA9IGVmZmVjdGl2bmVzcztcbiAgICB9XG4gICAgLyoqXG4gICAgICogQG92ZXJyaWRlXG4gICAgICogQHBhcmFtIHtDaGFyYWN0ZXJ9IGNoYXJhY3RlciBsZSBwZXJzb25uYWdlIHF1aSB1dGlsaXNlIGxlIG3DqWRvY1xuICAgICAqL1xuICAgIHVzZShjaGFyYWN0ZXIpIHtcbiAgICAgICAgaWYoY2hhcmFjdGVyLmhlYWx0aCA9PT0gMTAwKSB7XG4gICAgICAgICAgICByZXR1cm4gY2hhcmFjdGVyLm5hbWUgKyAnIGFscmVhZHkgaGFzIGZ1bGwgaGVhbHRoJztcbiAgICAgICAgfVxuICAgICAgICBjaGFyYWN0ZXIuaGVhbHRoICs9IHRoaXMuZWZmZWN0aXZuZXNzO1xuICAgICAgICBpZihjaGFyYWN0ZXIuaGVhbHRoID4gMTAwKSB7XG4gICAgICAgICAgICBjaGFyYWN0ZXIuaGVhbHRoID0gMTAwO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBzdXBlci51c2UoY2hhcmFjdGVyKTtcbiAgICB9XG59IiwiaW1wb3J0IHsgQ2hhcmFjdGVyIH0gZnJvbSBcIi4uL0NoYXJhY3RlclwiO1xuXG5leHBvcnQgY2xhc3MgQXJyb3dLZXkge1xuICAgIC8qKlxuICAgICAqIFxuICAgICAqIEBwYXJhbSB7Q2hhcmFjdGVyfSBjaGFyYWN0ZXIgbGUgcGVyc29ubmFnZSBhdXF1ZWwgb24gdmV1dCBhc3NpZ25lciBkZXMgY29udHLDtGxlcyBhdSBjbGF2aWVyXG4gICAgICovXG4gICAgY29uc3RydWN0b3IoY2hhcmFjdGVyKSB7XG4gICAgICAgIHRoaXMuY2hhcmFjdGVyID0gY2hhcmFjdGVyO1xuICAgIH1cblxuICAgIGluaXQoKSB7XG4gICAgICAgIC8qKlxuICAgICAgICAgKiBPbiBham91dGUgdW4gZXZlbnQgYXUga2V5ZG93biBzdXIgbGEgd2luZG93IChzdXIgbGUgZG9jdW1lbnQgw6dhXG4gICAgICAgICAqIG1hcmNoZSBhdXNzaSkuIE9uIGZhaXQgYXR0ZW50aW9uIMOgIGZhaXJlIHVuZSBmYXQgYXJyb3cgZnVuY3Rpb25cbiAgICAgICAgICogcG91ciBjb25zZXJ2ZXIgbGEgdmFsZXVyIGR1IHRoaXMgw6AgbCdpbnTDqXJpZXVyIGRlIGwnZXZlbnRcbiAgICAgICAgICovXG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdrZXlkb3duJywgZXZlbnQgPT4ge1xuICAgICAgICAgICAgLy9TZWxvbiBsYSB0b3VjaGUgcXVpIGVzdCBwcmVzc8OpZSwgb24gZMOpY2xlbmNoZSBsYSBtw6l0aG9kZVxuICAgICAgICAgICAgLy9tb3ZlIGF2ZWMgdW5lIHZhbGV1ciBvdSB1bmUgYXV0cmUgKHRlY2huaXF1ZW1lbnQgb24gcG91cnJhaXRcbiAgICAgICAgICAgIC8vcmVmYWN0b3Jpc2VyIMOnYSB1biBwZXUgbWlldXgsIG1haXMgw6dhIGxlIGZlcmEpXG4gICAgICAgICAgICBpZihldmVudC5rZXkgPT09ICdBcnJvd1VwJykge1xuICAgICAgICAgICAgICAgIHRoaXMuY2hhcmFjdGVyLm1vdmUoJ3VwJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBpZihldmVudC5rZXkgPT09ICdBcnJvd0Rvd24nKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jaGFyYWN0ZXIubW92ZSgnZG93bicpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYoZXZlbnQua2V5ID09PSAnQXJyb3dMZWZ0Jykge1xuICAgICAgICAgICAgICAgIHRoaXMuY2hhcmFjdGVyLm1vdmUoJ2xlZnQnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKGV2ZW50LmtleSA9PT0gJ0Fycm93UmlnaHQnKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jaGFyYWN0ZXIubW92ZSgncmlnaHQnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKGV2ZW50LmtleSA9PT0gJyAnKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jaGFyYWN0ZXIudGFrZUl0ZW0oKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmKGV2ZW50LmtleSA9PT0gJ2knKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5jaGFyYWN0ZXIudG9nZ2xlQmFja3BhY2soKTtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyh0aGlzLmNoYXJhY3Rlcik7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBcbiAgICAgICAgfSk7XG4gICAgfVxufSIsIi8qKlxuICogRW4gSlMgbW9kdWxhaXJlLCBwb3VyIHBvdXZvaXIgdXRpbGlzZXIgbGEgY2xhc3NlIENoYXJhY3RlclxuICogZGFucyBub3RyZSBmaWNoaWVyIGluZGV4LCBvbiBkb2l0IGQnYWJvcmQgaW1wb3J0ZXIgbGFcbiAqIGNsYXNzZSBlbiBxdWVzdGlvbiBkZSBzb24gZmljaGllclxuICovXG5pbXBvcnQgeyBDaGFyYWN0ZXIgfSBmcm9tIFwiLi9DaGFyYWN0ZXJcIjtcbmltcG9ydCB7IEFycm93S2V5IH0gZnJvbSBcIi4vY29udHJvbHMvQXJyb3dLZXlcIjtcbmltcG9ydCB7IEl0ZW0gfSBmcm9tIFwiLi9JdGVtXCI7XG5pbXBvcnQgeyBGb29kIH0gZnJvbSBcIi4vRm9vZFwiO1xuaW1wb3J0IHsgUGhhcm1hIH0gZnJvbSBcIi4vUGhhcm1hXCI7XG5cbmV4cG9ydCBjb25zdCB0aWxlU2l6ZSA9IDEwMDtcbi8vT24gZmFpdCB1bmUgaW5zdGFuY2UgZGUgcGVyc29ubmFnZVxubGV0IHBlcnNvMSA9IG5ldyBDaGFyYWN0ZXIoJ1BlcnNvIDEnKTtcbi8vVGFibGVhdSBjb250ZW5hbnQgdG91cyBsZXMgb2JqZXRzIGRlIG5vdHJlIGpldSBhY3R1ZWxcbmV4cG9ydCBjb25zdCBnYW1lT2JqZWN0cyA9IFtcbiAgICBwZXJzbzEsXG4gICAgbmV3IFBoYXJtYSgnRG9saXByb21lJywgNCwgMiwgMzApLFxuICAgIG5ldyBGb29kKCdicmVhZCcsIDUsIDUsIDEwMClcbl07XG4vL09uIGZhaXQgdW5lIGJvdWNsZSBwb3VyIGluaXRpYWxpc2VyIGxlIGRyYXcgZGUgdG91cyBsZXMgb2JqZXRzIGVuIHF1ZXN0aW9uXG5mb3IgKGNvbnN0IGl0ZXJhdG9yIG9mIGdhbWVPYmplY3RzKSB7XG4gICAgZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChpdGVyYXRvci5kcmF3KCkpXG59XG5cblxuLy9vbiBmYWl0IHVuZSBpbnN0YW5jZSBkZXMgY29udHLDtGxlcyBhdSBjbGF2aWVyIGVuIGx1aVxuLy9kb25uYW50IGwnaW5zdGFuY2UgZGUgcGVyc29ubmFnZSDDoCBjb250csO0bGVyXG5sZXQgYXJyb3dLZXkgPSBuZXcgQXJyb3dLZXkocGVyc28xKTtcbi8vT24gaW5pdGlhbGlzZSBsZXMgY29udHLDtGxlcyBhdSBjbGF2aWVyXG5hcnJvd0tleS5pbml0KCk7XG4vL09uIGZhaXQgdW4gZHJhdyBpbml0aWFsIHBvdXIgYXBwZW5kIGxlIGh0bWwgZHUgcGVyc29ubmFnZSBvw7kgb24gdmV1dFxuXG4iXSwic291cmNlUm9vdCI6IiJ9